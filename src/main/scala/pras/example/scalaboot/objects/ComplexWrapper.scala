/*
 * Module: BootScala, Project: SLI 
 * Copyright (C) Aviva UK Life, All rights reserved
 */
package pras.example.scalaboot.objects

import pras.example.scalaboot.objects.Complex._
/**
 * ComplexWrapper.scala - TODO singhp5 COMMENT MISSING.
 *
 * @author singhp5
 * @since 25 Sep 2013
 */
class ComplexWrapper(val self: Int) {
  def +(other: Imaginary) = {
    println(other)
    new ComplexNumber(self, other.j)
  }
  def -(other: Imaginary) = {
    println(other)
    new ComplexNumber(self, -other.j)
  }
}