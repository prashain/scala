import pras.example.scalaboot.objects._

object Test {

  

  implicit def numertoDigits(x: Int) = new { def toVectDigits(): Seq[Int] = x.toString map (Character.getNumericValue _) }
                                                  //> numertoDigits: (x: Int)AnyRef{def toVectDigits(): Seq[Int]}
  24.toVectDigits                                 //> res1: Seq[Int] = Vector(2, 4)

  import java.io._
  class FileWrapper(val file: java.io.File) {
    def /(next: String) = new FileWrapper(new java.io.File(file, next))
    override def toString = file.getCanonicalPath()
  }
  object FileWrapper {
    implicit def fileToWrapper(x: java.io.File) = new FileWrapper(x)
    implicit def unwrap(x: FileWrapper) = x.file
  }
  val file = new FileWrapper(new java.io.File("tmp"))
                                                  //> file  : Test.FileWrapper = C:\singhp5\installedApps\eclipseIndigo\tmp
  val tmp = file / "log.txt"                      //> tmp  : Test.FileWrapper = C:\singhp5\installedApps\eclipseIndigo\tmp\log.tx
                                                  //| t

  implicit def eachLineFile(x: java.io.File) = new { def eachLine(): Unit = println("works" + x) }
                                                  //> eachLineFile: (x: java.io.File)AnyRef{def eachLine(): Unit}
  implicit def eachLineFileW(x: FileWrapper) = new { def eachLine(): Unit = println("works" + x) }
                                                  //> eachLineFileW: (x: Test.FileWrapper)AnyRef{def eachLine(): Unit}
  val newFile = new java.io.File("tmp")           //> newFile  : java.io.File = tmp
  tmp.eachLine                                    //> worksC:\singhp5\installedApps\eclipseIndigo\tmp\log.txt


import pras.example.scalaboot.objects.Complex._

 (2+3.i )  + (2 -13.i)                            //> +i3
                                                  //| +i13
                                                  //| calling add on2+-13iand 2+3i
                                                  //| res2: pras.example.scalaboot.objects.Complex.ComplexNumber = 4+-10i

}