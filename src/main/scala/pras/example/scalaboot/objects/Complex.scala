/*
 * Module: BootScala, Project: SLI 
 * 
 */
package pras.example.scalaboot.objects

/**
 * Complex.scala - TODO singhp5 COMMENT MISSING.
 *
 * @author singhp5
 * @since 25 Sep 2013
 */
object Complex {
  class ComplexNumber(val real: Int, val im: Int) {

    def +(that: ComplexNumber): ComplexNumber = {
      println("calling add on" + that + "and " + this)
      new ComplexNumber(this.real + that.real, this.im + that.im)
    }

    def -(that: ComplexNumber): ComplexNumber = {
      println("calling subtraction on" + that)
      new ComplexNumber(real - that.real, im - that.im)
    }

    override def toString = real + "+" + im + "i"
  }
  class Imaginary(val j: Int) {
    override def toString = "+i" + j
  }

  implicit def any2complexadd(x: Int) = new ComplexWrapper(x)
  implicit def methodIm(x: Int) = new { def i(): Imaginary = new Imaginary(x) }

}
