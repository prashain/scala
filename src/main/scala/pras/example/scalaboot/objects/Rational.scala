/*
 * Module: BootScala, Project: SLI 
 * Copyright (C) Aviva UK Life, All rights reserved
 */
package pras.example.scalaboot.objects

import java.util.Date

/**
 * Rational.scala - TODO singhp5 COMMENT MISSING.
 *
 * @author singhp5
 * @since 20 Sep 2013
 */
class Rational(val num: Int, val den: Int) {

  def gcd(num: Int, den: Int): Int = {
    if (den == 0) num
    else gcd(den, num % den)
  }
  private val g: Int = gcd(num, den)

  val numer = num / g
  val denom = den / g

  def +(that: Rational): Rational = {
    new Rational((this.numer * that.denom) + (that.numer * this.denom), (this.denom * that.denom))
  }

  def *(that: Rational) = {
    new Rational(this.numer * that.numer, this.denom * that.denom)
  }
  override def toString = "[" + numer + "/" + denom + "]"

}

object Test1 {
  def main(args: Array[String]) {
    val rat = new Rational(3, 5) + new Rational(6, 8)
    println(rat)
    val product = new Rational(3, 5) * new Rational(6, 8)

    println(product)
  }
}