package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of 2 leaves") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 1))
    println("combination is  " + combine(leaflist))
    // assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3)))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }
  test("decode and encode a very short text with a larger tree") {
    new TestTrees {
      val tree = createCodeTree("adbadbdds".toList)
      assert(decode(tree, encode(tree)("abd".toList)) === "abd".toList)
    }
  }

  test("encode some text with frenchCode") {
    new TestTrees {
      val test = (encode(frenchCode)("encoredsecrettest".toList))
      val decodedTest = decode(frenchCode, test)
      assert(decodedTest == "encoredsecrettest".toList)
    }
  }
  test("decode and encode some longer text should be identity") {
    new TestTrees {
      val testDoc = "literature from 45 BC making it over  2000 years old".toList
      val codeTree = createCodeTree(testDoc)
      assert(decode(codeTree, encode(codeTree)(testDoc)) === testDoc)
    }
  }

  test("'createCodeTree(someText)' gives an optimal encoding, the number of bits when encoding 'someText' is minimal") {
    new TestTrees {
      //  val codeTree = createCodeTree("aaaaaaaabbbcdefgh".toList)
      // println(codeTree)
      val codeTree = createCodeTree("ettxxxx".toList)
      println(codeTree)
    }
  }
}
